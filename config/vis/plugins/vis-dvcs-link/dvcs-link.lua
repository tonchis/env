vis:command_register("dl", function(argv, force, win, selection, range)
    local command = "dvcs-link" .. " " .. vis.win.file.name .. " " .. selection.line

    local file = io.popen(command)
    local output = file:read()
    local success, msg, status = file:close()

    if status == 0 then 
        vis:info(output)
        file = io.popen(command .. " | vis-clipboard --copy")
        file:read()
        file:close()
    elseif status == 1 then
        vis:info(string.format("error" , command, status))
    end

    return true;
end)
