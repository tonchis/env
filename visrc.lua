-- The configuration options in this file are organized in the following sections:
--
-- * FILE TYPES
-- * THEME
-- * MAPPING: MOVEMENT
-- * MAPPING: CLIPBOARD
-- * MAPPING: EDITING
-- * MAPPING: OTHER
-- * BASIC OPTIONS
-- * TABS, SPACES AND INDENTATION
--

require("vis")
require("plugins/filetype")
require("plugins/textobject-lexer")
require("plugins/vis-dvcs-link/dvcs-link")
require("lexers/savi")

--
-- FILE TYPES
--

-- Redefine file extensions
vis.ftdetect.filetypes.ruby.ext        = { "%.rb$", "%.ru$" }
vis.ftdetect.filetypes.notes           = { ext = { "notes$", "%.notes$" } }
vis.ftdetect.filetypes.simple          = { ext = { "%.in$" } }
vis.ftdetect.filetypes.elm             = { ext = { "%.elm$" } }
vis.ftdetect.filetypes.objective_c.ext = { "%.mm$", "%.objc$" }
vis.ftdetect.filetypes.mercury         = { ext = { "%.m$" } }
vis.ftdetect.filetypes.prolog          = { ext = { "%.pro$" } }
vis.ftdetect.filetypes.rc.mime         = { "text/x-rc" }
vis.ftdetect.filetypes.bash            = { ext = { "^.?zshrc$", "^.?bashrc$", "%.sh$" } }
vis.ftdetect.filetypes.diff.ext        = { "^COMMIT_EDITMSG$", "%.diff$", "%.patch$" }
vis.ftdetect.filetypes.sql.ext         = { "%.sql", "%.cql" }
vis.ftdetect.filetypes.savi            = { ext = { "%.savi$" } }

-- Set the commenting pattern for each file type.
local comments = {
  ansi_c  = "//",
  bash    = "#",
  elm     = "--",
  lua     = "--",
  mercury = "%",
  ruby    = "#",
  shell   = "//",
  sql     = "--",
  go      = "//",
  savi    = "//",
  rust    = "//",
}

-- Functions and variables to commend and uncomment.
local comment_string

local comment = function()
  vis:feedkeys("m")
  vis:command("|prep " .. comment_string)
  vis:feedkeys("<Escape>M")
end

local uncomment = function()
  vis:feedkeys("m")
  vis:command("|trim " .. comment_string)
  vis:feedkeys("<Escape>M")
end

local move_line_up = function()
  -- Ignore up movement with multiple cursors.
  if (#vis.win.selections == 1) then
    vis:feedkeys("gk")
  end
end

local move_line_down = function()
  -- Ignore down movement with multiple cursors.
  if (#vis.win.selections == 1) then
    vis:feedkeys("gj")
  end
end

local spaces = 0

local show_spaces = function()
  spaces = spaces ~= 1 and 1 or 0
  vis:command("set show-spaces " .. spaces)
  vis:command("set show-tabs " .. spaces)
  vis:feedkeys("<Escape>")
end

vis.events.subscribe(vis.events.INIT, function()
  --
  -- THEME
  --
  vis:command("set theme zenburn")

  --
  -- MAPPINGS: MOVEMENT
  --

  -- Don't skip wrapped lines when moving up or down and disable moving multiple cursors.
  vis:map(vis.modes.NORMAL, "j", move_line_down)
  vis:map(vis.modes.NORMAL, "k", move_line_up)

  vis:command("map! normal l <vis-motion-line-char-next>")
  vis:command("map! normal h <vis-motion-line-char-prev>")
  vis:command("map! normal L g_")
  vis:command("map! normal H ^")

  vis:command("map! visual l <vis-motion-line-char-next>")
  vis:command("map! visual h <vis-motion-line-char-prev>")
  vis:command("map! visual L g_")
  vis:command("map! visual H ^")

  vis:command("map insert <C-e> <Escape>A")
  vis:command("map insert <C-a> <Escape>I")

  vis:command("map! insert <C-d> <vis-delete-char-next>")

  vis:command("unmap insert <C-h>")
  vis:command("map! insert <C-h> <vis-motion-line-char-prev>")
  vis:command("map! insert <C-l> <vis-motion-line-char-next>")

  -- Disable moving up or down on VISUAL mode.
  vis:command("map! visual j <vis-nop>")
  vis:command("map! visual k <vis-nop>")

  -- Move a whole line up or down in VISUAL LINE mode.
  vis:command("map! visual-line j <vis-motion-line-down>")
  vis:command("map! visual-line k <vis-motion-line-up>")

  -- Provide simple cursor reposition when typing opening-and-closing pairs of characters.
  vis:map(vis.modes.INSERT, '""', function()
    vis:feedkeys('" "' .. "<vis-motion-line-char-prev><vis-delete-char-prev>")
  end)

  vis:map(vis.modes.INSERT, "''", function()
    vis:feedkeys("' '" .. "<vis-motion-line-char-prev><vis-delete-char-prev>")
  end)

  vis:map(vis.modes.INSERT, "()", function()
    vis:feedkeys("( )" .. "<vis-motion-line-char-prev><vis-delete-char-prev>")
  end)

  vis:map(vis.modes.INSERT, "{}", function()
    vis:feedkeys("{ }" .. "<vis-motion-line-char-prev><vis-delete-char-prev>")
  end)

  vis:map(vis.modes.INSERT, "[]", function()
    vis:feedkeys("[ ]" .. "<vis-motion-line-char-prev><vis-delete-char-prev>")
  end)

  --
  -- MAPPING: CLIPBOARD
  --

  -- Clipboard mappings: Y copy.
  vis:command('map! visual Y \\"*y')
  vis:command('map! visual-line Y \\"*y')

  -- Clipboard mappings: <M-c> copy.
  vis:command('map! visual <M-c> \\"*y')
  vis:command('map! visual-line <M-c> \\"*y')

  -- Clipboard mappings: <M-v> paste.
  vis:command('map! normal <M-v> \\"*p')

  --
  -- MAPPING: EDITING
  --

  -- Unmap increment and decrement number.
  vis:command("unmap normal <C-a>")
  vis:command("unmap normal <C-x>")

  -- Comment lines with Option + 1.
  vis:map(vis.modes.VISUAL, "<M-1>", comment)
  vis:command("map normal <M-1> <vis-mode-visual-linewise><M-1>")

  -- Uncomment lines with Option + 2.
  vis:map(vis.modes.VISUAL, "<M-2>", uncomment)
  vis:command("map normal <M-2> <vis-mode-visual-linewise><M-2>")

  -- Surround text
  local surround = {
    ["'"] = {"'" , "'"},
    ['"'] = {'"' , '"'},
    ["("] = {"( ", " )"},
    [")"] = {"(" , ")"},
    ["<"] = {"< ", " >"},
    [">"] = {"<" , ">"},
    ["["] = {"[ ", " ]"},
    ["]"] = {"[" , "]"},
    ["{"] = {"{ ", " }"},
    ["}"] = {"{" , "}"},
    ["*"] = {"*" , "*"},
    ["_"] = {"_" , "_"},
    ["`"] = {"`" , "`"},
    [" "] = {" " , " "},
  }

  vis:map(vis.modes.VISUAL, "S", function(keys)
    if #keys < 1 then
      return -1
    end

    if surround[keys] then
      local opening = surround[keys][1]
      local closing = surround[keys][2]

      vis:feedkeys(":{i/" .. opening .."/ a/" .. closing .. "/}")
      vis:feedkeys("<Enter><Escape><Escape>")
    end

    return #keys
  end, "Surround text")

  vis:map(vis.modes.INSERT, "<M-p>", function(keys)
    vis:feedkeys('require "pry"; binding.pry')

    return #keys
  end, "Insert `require \"pry\"; binding.pry`")

  -- Remove trailing whitespaces
  vis:map(vis.modes.NORMAL, "<M-w>", function()
    vis:feedkeys(":x/[\t ]+$/ d<Enter>")
  end)

  --
  -- MAPPING: OTHER
  --

  -- Display invisible characters with ,
  vis:command("unmap normal ,")
  vis:map(vis.modes.NORMAL, ",", show_spaces)

  -- Ignore non-mapped meta or control combinations
  vis.events.subscribe(vis.events.INPUT, function(keys)
    local prefix = keys:sub(1, 3)
    if prefix == "<M-" or prefix == "<C-" then
      return true
    end
  end)

  -- Cheap fuzzy finder
  vis:command_register("fe", function(argv, force, win, selection, range)
    local file = io.popen("rg -l . | vis-menu")
    local output = file:read()
    local success, msg, status = file:close()

    if status == 0 then
      local open_command = force and "vs" or "e"
      vis:feedkeys(string.format(":%s %s<Enter>", open_command, output))
    end

    vis:feedkeys("<vis-redraw>")

    return true
  end)
end)

vis.events.subscribe(vis.events.WIN_OPEN, function(win)
  --
  -- BASIC OPTIONS
  --

  vis:command("set number")
  vis:command("set colorcolumn 100")
  vis:command("set cursorline")
  vis:command("set autoindent")

  comment_string = comments[win.syntax] or "#"

  --
  -- TABS, SPACES AND INDENTATION
  --

  if (win.syntax == "ansi_c") then
    vis:command("set tabwidth 4")
    vis:command("set expandtab no")
  elseif (win.syntax == "makefile") then
    vis:command("set tabwidth 4")
    vis:command("set expandtab no")
  elseif (win.syntax == "go") then
    vis:command("set tabwidth 4")
    vis:command("set expandtab no")
  elseif (win.syntax == "rust") then
    vis:command("set tabwidth 4")
    vis:command("set expandtab yes")
  else
    vis:command("set tabwidth 2")
    vis:command("set expandtab yes")
  end

  vis:command("set autoindent yes")
end)
