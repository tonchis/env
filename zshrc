#! /bin/zsh

export LC_CTYPE=en_US.UTF-8
export LC_NUMERIC=en_US.UTF-8
export LC_TIME=en_US.UTF-8
export LC_COLLATE=en_US.UTF-8
export LC_MONETARY=en_US.UTF-8
export LC_MESSAGES=en_US.UTF-8

autoload -Uz colors
colors

# VARIABLES

RED="%{$fg[red]%}"
GREEN="%{$fg[green]%}"
BLUE="%{$fg[blue]%}"
GREY="%{$fg[grey]%}"
MAGENTA="%{$fg[magenta]%}"
CYAN="%{$fg[cyan]%}"
WHITE="%{$fg[white]%}"
YELLOW="%{$fg[yellow]%}"
BLACK="%{$fg[black]%}"

BRED="%{$fg_bold[red]%}"
BGREEN="%{$fg_bold[green]%}"
BBLUE="%{$fg_bold[blue]%}"
BGREY="%{$fg_bold[grey]%}"
BMAGENTA="%{$fg_bold[magenta]%}"
BCYAN="%{$fg_bold[cyan]%}"
BWHITE="%{$fg_bold[white]%}"
BYELLOW="%{$fg_bold[yellow]%}"

RESET="%{$reset_color%}"

setopt null_glob

# Keep lots of history within the shell and save it to ~/.zsh_history:
setopt SHARE_HISTORY
setopt INC_APPEND_HISTORY
setopt HIST_SAVE_NO_DUPS
export HISTSIZE=65536
export SAVEHIST=60000
export HISTFILESIZE=$HISTSIZE
export HISTFILE=~/.zsh_history

# Allow # comments in zsh CLI
setopt interactivecomments

# Use modern completion system
# Find docker completion in ~/.zsh/completion
fpath=(~/.zsh/completion $fpath)
autoload -Uz compinit && compinit -i

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

# Tonchis custom stuff
# Config according to OS.
if [ "`uname`" = 'Darwin' ]; then
  export LSCOLORS="gxfxcxdxbxegedabagacad"
  export COLORTERM=xterm-color256
else
  export LS_COLORS='di=36:fi=0:ln=31:pi=5:so=5:bd=5:cd=5:or=31:mi=0:ex=35'
  export COLORTERM=gnome-256color
fi

export EDITOR="vise"

# Binded ^R to history search, like bash.
bindkey '^R' history-incremental-pattern-search-backward

# This is to cycle through results when searching.
bindkey -M viins '^F' history-incremental-pattern-search-forward
bindkey -M viins '^R' history-incremental-pattern-search-backward

# To enable ^E and ^A macros.
bindkey -M viins '^A' vi-beginning-of-line
bindkey -M viins '^E' vi-end-of-line

# Completion options
# zstyle ':completion:*' completer _ignored _approximate
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
zstyle ':completion:*' list-prompt '%SAt %p: Hit TAB for more, or the character to insert%s'
zstyle ':completion:*' matcher-list '' 'r:|[._-]=** r:|=**'
zstyle ':completion:*' menu 'select=1'
zstyle ':completion:*' original 'true'
zstyle ':completion:*' select-prompt '%SScrolling active: current selection at %p%s'
zstyle ':completion:*' verbose 'true'
zstyle ':completion:*:descriptions' format "${CYAN}Completing %d${RESET}"
zstyle ':completion:*' group-name ''
zstyle ':completion:*' group-order \
        local-directories builtins aliases commands

# ALIASES

# Global
alias reload="source ~/.zshrc; clear"
alias e="$EDITOR"
alias ec="$EDITOR ~/env/zshrc"
alias ee="$EDITOR ~/env/ensure-manifest"
alias ..="cd .."
alias ~="cd ~/"
alias quit="exit"
alias ll="ls -lsah"
alias pde="pushd ~/env"

if [ "`uname`" = 'Linux' ]; then
  alias ls="ls --color=always"
else
  alias ls="ls -G"
fi

alias tree="tree -C -I node_modules"

# Git
alias gout="git checkout"
alias gca="git commit --amend -C HEAD"
alias ga="git add"
alias gs="git status"
alias gb="git branch"
alias gba="git branch -a"
alias getch="git fetch --prune"
alias gg="git log --decorate --oneline --all --graph -10"
alias gga="git log --decorate --oneline --all --graph"
alias ggf="git log --decorate --oneline --all --graph --first-parent"
alias grb="git rebase"
alias gr="git reset"
alias grs="git restore --staged"
alias gd="git diff --color-words"
alias gdc="git diff --color-words --cached"
alias gsh="git show"
alias gm="git merge"
alias gsp="git stash pop"
alias gundo="git reset --soft HEAD~1"
alias current-branch="git symbolic-ref --short HEAD"
alias grbom="git rebase origin/master master"
alias gap="git add -p"
alias gcw="gc wip"
alias gpl="git push --force-with-lease"

# Tmux
alias t="tmux"
alias ta="tmux attach -t"
alias ts="tmux new-session -s"

# Docker
alias d="docker"
alias dc="docker-compose"
alias ds="docker -H 'ssh://docker-swarm-01'"

# Workflow
alias cs="crote e standup"

# Rust
alias c="cargo"

# Custom functions
function gc() {
  local commit_file=$(mktemp -t `basename $PWD`.XXXXXXXXX)

  if [[ "$1" == "wip" ]]; then
    echo -n "[WIP] " >> $commit_file
    shift
  fi

  if [[ $# -gt 0 ]]; then
    echo "$@" >> $commit_file
    echo "" >> $commit_file
  fi

  git commit -e -v -F $commit_file
}

function gash() {
  git stash save -u "$*"
}

function gbf() {
  local dev_branch=$(current-branch)

  git stash && \
  git fetch --prune && \
  git checkout main && \
  git rebase && \
  git branch -d $dev_branch && \
  git stash pop
}

function rb-env() {
  echo "which ruby    $(which ruby)"
  echo "gem env home  $(gem env home)"
}

function chrb() {
  chruby `cat .ruby-version`
}

function show() {
  if [[ $(type $1) =~ "shell function" ]] || [[ $(type $1) =~ "alias" ]]; then
    which $1
  else
    less -RFX $(which $1)
  fi
}

function vag() {
  ag -l $@ | xargs -o vim -p "+/$1" "+:set hlsearch!"
}

function vc() {
  git status --short | ag "UU .*" | awk '{ print $2 }' | xargs -o vim -p "+/<<<<" "+:set hlsearch!"
}

function gpb() {
  git push -u origin `current-branch` && current-branch | pbcopy
}

function ssh-add-key() {
  if [[ -n $(ssh-add -l | awk -v full_path="$1" 'full_path == $3') ]]; then
    echo "Key already added."
    return
  else
    echo "Adding key:"
    ssh-add $1
  fi
}

# Use Git's colored diff when available
hash git &>/dev/null
if [ $? -eq 0 ]; then
  function diff() {
    git diff --no-index --color-words "$@"
  }
fi

# Project specific aliases: M2X
function m2x-ssh-knock() {
  local m2x_ops_dir="/Users/tonchis/code/citrusbyte/att-iot-ops-cli"

  (cd "$m2x_ops_dir"; GEM_PATH="$m2x_ops_dir/.gs" bin/m2x-ops knock -a)
}

function m2x-ops() {
  local m2x_ops_dir="/Users/tonchis/code/citrusbyte/att-iot-ops-cli"

  (cd "$m2x_ops_dir"; GEM_PATH="$m2x_ops_dir/.gs" bin/m2x-ops $*)
}

# Project specific aliases: RMM
function deploy-dev() {
  getch && gout $1 && grb && mina dev deploy target=$1
}

##
# PROMPT

# get the name of the branch or commit (short SHA) we are on
function git_prompt_info() {
  ref=$(git symbolic-ref --short HEAD 2> /dev/null) || ref=$(git rev-parse --short HEAD 2> /dev/null) || return
  echo "${ref}"
}

# PS1

setopt promptsubst

local full_path='${CYAN}%~'
local git_stuff='${RESET}$(git_prompt_info)'

local context="${full_path} ${git_stuff}"
local insert_dollar_sign="%B$%b "
local normal_dollar_sign="%B${YELLOW}\$${RESET}%b "

# Use vim keybindings.
bindkey -v
export KEYTIMEOUT=1

# Change the color of the $ when in normal mode.
function zle-line-init zle-keymap-select {
  if [ "$KEYMAP" = "vicmd" ]; then
    PROMPT="$context"$'\n'"$normal_dollar_sign"
  else
    PROMPT="$context"$'\n'"$insert_dollar_sign"
  fi

  zle reset-prompt
}
zle -N zle-line-init
zle -N zle-keymap-select

# By default in vi, the backspace when entering insert mode after a `c*` command
# does not delete characters. This fixes that.
bindkey "^?" backward-delete-char

# PATH

# set GOPATH
export GOPATH="$HOME/code/go"

# Custom binaries
export PATH=~/bin:$PATH

# Homebrew's bin path and SHELL location depending on M1 or not.
if [[ `uname -m` = "arm64" ]]; then
  export SHELL=/opt/homebrew/bin/zsh
  export PATH=/opt/homebrew/sbin:/opt/homebrew/bin:$PATH
else
  export SHELL=/usr/local/bin/zsh
  export PATH=/usr/local/sbin:/usr/local/bin:$PATH
fi

# brew info coreutils
export PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"

# yarn global
export PATH=~/.yarn/bin:$PATH

export PATH="$GOPATH/bin":$PATH

export PATH="/usr/local/opt/node@8/bin":$PATH

export PATH="$HOME/.cargo/bin":$PATH

export PATH="$HOME/.pyenv/versions/3.8.2/bin":$PATH

export PATH="$HOME/.poetry/bin":$PATH

# CHRUBY

CHRUBY_SCRIPT="/usr/local/opt/chruby/share/chruby/chruby.sh"
if [[ -f $CHRUBY_SCRIPT ]]; then
  source $CHRUBY_SCRIPT
  export RUBY_MANAGER='chruby'
fi

if [[ -f ~/.ruby-version ]]; then
  RUBY_DEFAULT_VERSION=$(cat ~/.ruby-version)
  if [[ -n $RUBY_DEFAULT_VERSION ]]; then chruby $RUBY_DEFAULT_VERSION; fi
fi

# OPAM configuration
. /Users/tonchis/.opam/opam-init/init.zsh > /dev/null 2> /dev/null || true

# GPG
export GPGKEY=363BC855

# Running yEd without lag issues.
# See http://yed.yworks.com/support/qa/7039/horribly-slow-on-macbook-pro-15-retina-display
alias yed="java -jar ~/code/forks/yed-3.14.1/yed.jar &"

if [[ -f ~/.homebrew_github_api_token ]]; then
  export HOMEBREW_GITHUB_API_TOKEN=$(cat ~/.homebrew_github_api_token)
fi

# Make `less` quit if the output has only one page (-FX).
# Make `less` parse ansii colors (-R).
export LESS="-RFX"

# vis configuration variables
export VIS_PATH=$HOME

# Load secrets.
if [[ -f ~/.zshrc_private ]]; then; . ~/.zshrc_private; fi

# Crote completion function
# Ref: https://mads-hartmann.com/2017/08/06/writing-zsh-completion-scripts.html
_crote() {
  local line

  _arguments -C \
    "-h[Show help]" \
    "--h[Show help]" \
    "1:actions:(list edit delete)" \
    "*::arg:->args"

  if [ $line[1] ]; then
    _files -W ~/.crote
  fi
}
compdef _crote crote

# Load asdf
source $(brew --prefix asdf)/libexec/asdf.sh
