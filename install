#!/bin/bash

set -e

EXCLUDE_FILES=(README.md LICENSE install ensure-manifest visrc.lua ^config .gitmodules)

# Joins excluded files with |
SAVE_IFS=$IFS
IFS="|"
EXCLUDE_FILES="${EXCLUDE_FILES[*]}"
IFS=$SAVE_IFS

FILES=$(git ls-files | grep -v -E "$EXCLUDE_FILES")
echo "## Linking dotfiles"
echo ""
for FILE in $FILES; do
  echo "$FILE -> $HOME/.$FILE"
  ln -sf "$PWD/$FILE" "$HOME/.$FILE"
done
echo ""

DIRS=$(ls config)
echo "## Linking directories"
echo ""
if [ ! -d $HOME/.config ]; then
  echo "Creating $HOME/.config"
  mkdir "$HOME/.config"
fi
for DIR in $DIRS; do
  echo "config/$DIR -> $HOME/.config/$DIR"
  ln -sF "$PWD/config/$DIR" "$HOME/.config"
done
echo ""

echo "## Linking files"
echo ""

echo "ensure-manifest -> $HOME/.ensure/manifest"
ln -sf "$PWD/ensure-manifest" "$HOME/.ensure/manifest"

echo "visrc.lua -> $HOME/visrc.lua"
ln -sf "$PWD/visrc.lua" "$HOME/visrc.lua"

echo "gpg-agent.conf -> $HOME/.gnupg/gpg-agent.conf"
if [[ $(uname -m) == "arm64" ]]; then
  ln -sf "$PWD/gpg-agent.conf.arm64" "$HOME/.gnupg/gpg-agent.conf"
else
  ln -sf "$PWD/gpg-agent.conf.x86_64" "$HOME/.gnupg/gpg-agent.conf"
fi
echo ""

echo "Done"
